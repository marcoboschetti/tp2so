/*Tiene una funcion dibujo(n) que me dibuja el hombrecito segun la cantidad de vidas). NO tenia sentido hacerle una libreria.... perdon!*/
#include "defs.h"
#include "sys_calls.h"

static int WORD=5;
static int j;/*Es la cantidad de letras que hay en TIRADAS*/
int Jugar(char *palabra,int vidas);
int CheckeoLetra(char*palabra, char *acertadas, char *tiradas, int largo);
void ImprimoEstado(char *palabra,char *tiradas,int vida,int largo);
char getChar();
void dibujo(int vida);

void hangman(void) {

clear();
j=0;
char *secreto,*palabras[]={"INTERRUPCIONES","KERNEL","USERLAND","DRIVERS","KEYBOARDBUFFER","BOOTLOADER","ARQUITECTURA","INTEGRADO","PILOTO","DECODIFICADOR"};
int aux,vidas=7;
secreto=palabras[aux=WORD];
WORD=((WORD+1)%9);
printf(12, "Palabra Iniciada, es la #%d\n %d vidas cargadas\n",aux,vidas);
aux=Jugar(secreto,vidas);
if (aux==0)
        printf(12, "Perdiste! La palabra era %s\n",secreto);
else{
        printf(12, "La palabra era %s. Ganaste, Felicitaciones!\n",secreto);
    }
printf(10,"Para salir del juego, ingrese cualquier tecla");
while(!sys_read_char());
clear();
return;
}

int Jugar(char *palabra,int vidas){
int i,largo=strlen(palabra),aux,letrasbien=0;
char tiradas[26];
char acertadas[26];
for(i=0;i<largo;i++)
        acertadas[i]='_';
while((vidas>0)&&(letrasbien!=largo)){
        updateFooter();
        updateShellTime();
        ImprimoEstado(acertadas,tiradas,vidas,largo);
        printf(12, "Letras bien: %d de %d. ",letrasbien,largo);
        aux=CheckeoLetra(palabra,acertadas,tiradas,largo);
        if(aux==0)
                vidas-=1;
        for(letrasbien=0,i=0;i<largo;i++)
                {if(isalpha(acertadas[i]))
                        letrasbien++;}
        clear();
        }
return vidas;
}

void ImprimoEstado(char *palabra,char *tiradas,int vida,int largo)
{int i;
dibujo(vida);
printf(12, "                      ");
for(i=0;i<largo;i++)/*Imprime el string mas... lindo*/
        printf(12, "%c ",palabra[i]);
printf(12, "\n\n Letras tiradas:  ");
for(i=0;i<26;i++)
        if(isalpha(tiradas[i]))
                printf(12, "%c - ",tiradas[i]);
printf(12, "\n");
}

int/*Devuelve 1 si la letra estaba, la agrega a ACERTADAS. Si no, devuelve 0 y la agrega a TIRADAS*/
CheckeoLetra(char*palabra, char *acertadas, char *tiradas, int largo)
{char c;
int i,flag,flag2;
printf(12, "Tira una letra:\n");
do
c=toUpper(getChar());
while(!isalpha(c));
while(getChar()!='\n');
for(i=0,flag=0;i<largo;i++)
        {if (palabra[i]==c)
                {flag=1;
                acertadas[i]=c;
                }
        }
if(flag==0)
        {for(i=0,flag2=0;i<j;i++)
                {if(c==tiradas[i])
                        flag2=1;}
        if (flag2==0)
                tiradas[j++]=c;
        else
                flag=1;/*La letra ya la habias tirado mal, no te vuelvo a sacar vidas!*/
        }
    return flag;
}

char getChar(){
	char c;
	while(!(c=sys_read_char())){
        updateShellTime();
    }
    sys_write_char(c,10);
	return c;
}
