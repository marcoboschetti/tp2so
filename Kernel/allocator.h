#ifndef ALLOCATOR_H
#define ALLOCATOR_H

#include <stdint.h>

uint64_t * getPage();
uint64_t * getPhysicalPage();
void freePage(void* page);
void freePhysicalPage(void* page);

#endif