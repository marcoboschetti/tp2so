#include "./asm/read_from_keyboard.h"

#define vec_size 256
#define ARRIBA 0xE048

static char vector[vec_size][3] = {};
#include <naiveConsole.h>
#include "defs.h"

static char user_keyboard_buffer[vec_size] = {0};
static char kernel_keyboard_buffer[vec_size] = {0};

static int ur=0;
static int uw=0;
static int kr=0;
static int kw=0;

char kernelWantsChar(char c);

// shift=1 <-- prendido
// shift=0 <-- apagado
static char shift=0;

// bloq_mayus=1 <-- prendido
// bloq_mayus=0 <-- apagado
static char bloq_mayus=0;

// altgr=1 <-- prendido
// altgr=0 <-- apagado
static char altgr=0;

void get_char(){
	unsigned char get=read_from_keyboard();
	if(get==0x3A){
		bloq_mayus=1-bloq_mayus;
	}
	else if(get==0x2A || get==0x36){
		shift=1;
	}
	else if(get==0xAA || get==0xB6){
		shift=0;
	}
	else if(get==0x38){
		altgr=1;
	}
	else if(get==0xB8){
		altgr=0;
	}
	else if(kernelWantsChar(get)){
		kernel_keyboard_buffer[kw++]=get;
		if(kw==(vec_size-1)){
			kw=0;
		}
	}else if(get<0x80){
		user_keyboard_buffer[uw++]=codeToAscii(get);
		if(uw==(vec_size-1)){
			uw=0;
		}
	}
}

char user_give_char(){

	while(!isForeground()){
		yield();
//		return 0;
	}

	char to_return;
	if(uw==ur){
		to_return = 0;
	}else{
		to_return = user_keyboard_buffer[ur++];
		if(ur==(vec_size-1)){
			ur=0;
		}
	}
	return to_return;
}

char kernel_give_char(){
	char to_return;

	if(kw==kr){
		to_return = 0;
	}else{
		to_return = kernel_keyboard_buffer[kr++];
		if(kr==(vec_size-1)){
			kr=0;
		}
	}
	return to_return;
}

char codeToAscii(char code){
	char ascii;
	if(shift ^ bloq_mayus)
		ascii= vector[code][1];
	else if(altgr)
		ascii=vector[code][2];
	else
		ascii=vector[code][0];
	return ascii;
}

void initialize_keyboard(){
	for(int i=0;i<256;i++){
		vector[i][0]=0;
		vector[i][1]=0;
		vector[i][2]=0;
	}

	vector[0x0D][0]='¿';
	vector[0x0D][1]='¡';
	vector[0x0D][2]=0;


	vector[0x1A][0]=0;
	vector[0x1A][1]=0;
	vector[0x1A][2]=0;

	vector[0x1B][0]='+';
	vector[0x1B][1]='*';
	vector[0x1B][2]='~';

//Ñ
	vector[0x27][0]=0;
	vector[0x27][1]=0;
	vector[0x27][2]=0;

	vector[0x28][0]='{';
	vector[0x28][1]='[';
		vector[0x28][2]='^';

		vector[0x29][0]='|';
		vector[0x29][1]='°';
		vector[0x29][2]='¬';

		vector[0x2B][0]='}';
		vector[0x2B][1]=']';
vector[0x2B][2]=0;

vector[0x33][0]=',';
vector[0x33][1]=';';
vector[0x33][2]=0;


vector[0x34][0]='.';
vector[0x34][1]=':';
vector[0x34][2]='·';


vector[0x0C][1]='?';
vector[0x0C][0]='\'';
vector[0x0C][2]='\\';



vector[0x1E][0]='a';
vector[0x30][0]='b';
vector[0x2E][0]='c';
vector[0x20][0]='d';
vector[0x12][0]='e';
vector[0x21][0]='f';
vector[0x22][0]='g';
vector[0x23][0]='h';
vector[0x17][0]='i';
vector[0x24][0]='j';
vector[0x25][0]='k';
vector[0x26][0]='l';
vector[0x32][0]='m';
vector[0x31][0]='n';
vector[0x18][0]='o';
vector[0x19][0]='p';
vector[0x10][0]='q';
vector[0x13][0]='r';
vector[0x1F][0]='s';
vector[0x14][0]='t';
vector[0x16][0]='u';
vector[0x2F][0]='v';
vector[0x11][0]='w';
vector[0x2D][0]='x';
vector[0x15][0]='y';
vector[0x2C][0]='z';
vector[0x2][0]='1';
vector[0x3][0]='2';
vector[0x4][0]='3';
vector[0x5][0]='4';
vector[0x6][0]='5';
vector[0x7][0]='6';
vector[0x8][0]='7';
vector[0x9][0]='8';
vector[0x0A][0]='9';
vector[0x0B][0]='0';
vector[0x35][0]='-';

vector[0x1E][1]='A';
vector[0x30][1]='B';
vector[0x2E][1]='C';
vector[0x20][1]='D';
vector[0x12][1]='E';
vector[0x21][1]='F';
vector[0x22][1]='G';
vector[0x23][1]='H';
vector[0x17][1]='I';
vector[0x24][1]='J';
vector[0x25][1]='K';
vector[0x26][1]='L';
vector[0x32][1]='M';
vector[0x31][1]='N';
vector[0x18][1]='O';
vector[0x19][1]='P';
vector[0x10][1]='Q';
vector[0x13][1]='R';
vector[0x1F][1]='S';
vector[0x14][1]='T';
vector[0x16][1]='U';
vector[0x2F][1]='V';
vector[0x11][1]='W';
vector[0x2D][1]='X';
vector[0x15][1]='Y';
vector[0x2C][1]='Z';
vector[0x2][1]='!';
vector[0x3][1]='"';
vector[0x4][1]='#';
vector[0x5][1]='$';
vector[0x6][1]='%';
vector[0x7][1]='&';
vector[0x8][1]='/';
vector[0x9][1]='(';
	vector[0x0A][1]=')';
vector[0x0B][1]='=';
vector[0x35][1]='/';

vector[0x10][2]='@';
vector[0x56][0]='<';
vector[0x56][1]='>';
vector[0x56][2]='|';

vector[0x0E][0]=0x0E;
vector[0x0E][1]=0x0E;
vector[0x0E][2]=0x0E;
vector[0x1C][0]=10;
vector[0x1C][1]=10;
vector[0x1C][2]=10;
vector[0x39][0]=' ';
vector[0x39][1]=' ';
vector[0x39][2]=' ';

return;
}


char kernelWantsChar(char c){
	// Checks if c is in [ F1 , F10]
	return (c == 0x3b || c == 0x3c || c ==  0x3d || c == 0x3e || c == 0x3f || c ==  0x40 || c ==  0x41 || c ==  0x42 || c ==  0x43  || c ==  0x44 );
//	return 0;
}