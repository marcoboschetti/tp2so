#ifndef TYPES_H_
#define TYPES_H_
#include <stdint.h>

#pragma pack(push)
#pragma pack (1) 		/* Alinear las siguiente estructuras a 1 byte */

/* Descriptor de interrupcion */
typedef struct {
  uint16_t offsetLow;
  uint16_t selector;
  uint8_t zero1;
  uint8_t type_attr;
  uint16_t offsetMid;
  uint32_t offsetHigh;
  uint8_t zero2;
  uint8_t zero3;
  uint8_t zero4;
  uint8_t zero5; 
}IDTEntry;

#pragma pack(pop)

#endif /* TYPES_H_ */
