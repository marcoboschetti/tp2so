#include <stdint.h>
#include <string.h>
#include <lib.h>
#include <moduleLoader.h>
#include <naiveConsole.h>
#include "./interrupts.h"
#include "types.h"
#include "defs.h"
#include "./asm/Time.h"
#include "./allocator.h"
#include "./scheduler.h"

/*
char sys_read_char(); <-1
void sys_write_char(char c); <-2
void sys_write_string(char * string, char form);<-3
void clear(); <-4
void set_ssaver_time(long seconds); <-5
char get_time(char selector);<-6
void set_time(char selector,char value);<-7
int sys_get_video_x(); <-8
int sys_get_video_y(); <-9
void sys_set_video_x(); <-10
void sys_set_video_y(); <-11
void sys_video_limit_scroll(int x, int y); <-12
void cli();<-13
void sti();<-14
void* getPage();<-15
void freePage(void* page);<-16
Task * CreateProcess(TaskFunc func, const char *name);<-17
char** listProcess();<-18
void DeleteProcess(int pid);<-19
void waitPid(int pid);<-20
void wait(int secs);<-21
void yield(); <-22
void toForeground(int pid); <-23
void sem_open(int id); <- 24
void sem_up(int id); <- 25
void sem_down(int id); <- 26
void sem_close(int id); <- 27
void * get_shared_memory(int id); <-28
void close_shared_memory(int id); <- 29
void listIpcs(PrintIpc ** buffer); <-30
*/
void sys_call_selector(uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t rcx){
	switch(rdi){
		case 1:
		user_give_char();
		return;
		case 2:
		sys_write_char(rsi,rdx);
		return;
		case 3:
		sys_write_string(rsi,rdx);
		return;
		case 4:
		clearScreen();
		return;
		case 6:
		get_time(rsi);
		return;
		case 7:
		set_time(rsi,rdx);
		return;
		case 8:
		getX();
		return;
		case 9:
		getY();
		return;
		case 10:
		setX(rsi);
		return;
		case 11:
		setY(rsi);
		return;
		case 12:
		set_video_scrollLimit(rsi,rdx);
		return;
		case 13:
		_cli();
		return;
		case 14:
		_sti();
		return;
		case 15:
		getPage();
		return;
		case 16:
		freePage(rsi);
		return;
		case 17:
		KnewTask(rsi,rdx,rcx);
		return;
		case 18:
		listProcess(rsi);
		return;
		case 19:
		deleteProcess(rsi);
		return;
		case 20:
		waitPid(rsi);
		return;
		case 21:
		wait(rsi);
		return;
		case 22:
		yield();
		return;
		case 23:
		toForeground(rsi);
		return;
		case 24: 
		sem_open(rsi);
		return;
		case 25:
		sem_up(rsi);
		return;
		case 26: 
		sem_down(rsi);
		return;
		case 27:
		sem_close(rsi);
		return;
		case 28:
		get_shared_memory(rsi);
		return;
		case 29:
		close_shared_memory(rsi);
		return;
		case 30:
		listIpcs(rsi);
		return;
		default: sys_write_string("Error 1002: INTERRUPCION INVALIDA",12);
	}
}
