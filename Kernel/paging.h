
#include <stdint.h>

static const uint64_t PageSize = 0x1000;

/*
static uint64_t * PML4 = (uint64_t *) 0x201000;
static uint64_t * PDPT = (uint64_t *) 0x202007;
static uint64_t * PDT = (uint64_t *) 0x203007;
static uint64_t * PT = (uint64_t *) 0x204007;
static uint64_t * PT2 = (uint64_t *) 0x205007;
*/

/* Cada tabla es de 4K, conteniendo entradas de 64bits(8 bytes) -> Tiene 512 entradas*/
static uint64_t * PML4 = (uint64_t *) 0x700000;
static uint64_t * PDPT = (uint64_t *) 0x701000; //La base es 0x701000, pero en el puntero de PML4, se le pasa con un 7 al final.
static uint64_t * PDT = (uint64_t *) 0x702000;
static uint64_t * PT = (uint64_t *) 0x703000;

typedef struct{

} PML4_entry;

typedef struct{

} CR3_entry;

void setCR3();
void refreshCR3();
void initializePaging();