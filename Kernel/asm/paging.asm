GLOBAL setCR3
GLOBAL setPML4
GLOBAL setPDPE
GLOBAL setPDE
GLOBAL refreshCR3

extern ncPrintHex
extern ncPrint

%macro pushaq 0
    push rax      ;save current rax
    push rbx      ;save current rbx
    push rcx      ;save current rcx
    push rdx      ;save current rdx
    push rbp      ;save current rbp
    push rdi       ;save current rdi
    push rsi       ;save current rsi
    push r8        ;save current r8
    push r9        ;save current r9
    push r10      ;save current r10
    push r11      ;save current r11
    push r12      ;save current r12
    push r13      ;save current r13
    push r14      ;save current r14
    push r15      ;save current r15
    push fs
    push gs
%endmacro

%macro popaq 0
    pop gs
    pop fs
    pop r15
    pop r14
    pop r13
    pop r12
    pop r11
    pop r10
    pop r9
    pop r8
    pop rsi
    pop rdi
    pop rbp
    pop rdx
    pop rcx
    pop rbx
    pop rax

%endmacro


test : db "a",0

;sorprendentemente, setea CR3
;setCR3:
	; load CR3 new dir
	pushaq

	mov rax, cr3

	and rax, 0xFFF0000000000FFF
	or rax, rdi

	mov cr3, rax	

	popaq

	ret

setPML4:
	;0000 0000 0000()0000 0001	0111b
	;0x000 0040000000 0 1 7
	mov rax, 0x000020300017
;	or	rax, 0x0000000020300000 ;PDPE
	mov QWORD [rdi], rax

	ret

setPDPE:
	;0000 0000 0000()0000 0001	0111b
	;0x000 0040000000 0 1 7
	mov QWORD rax, 0x000020500017
;	or 	rax, 0x0000000020500000; PDE
	mov QWORD [rdi], rax

	ret

setPDE:
	mov rax, 11000011b
	or	rax, rsi

	mov QWORD [rdi], rax
	
	

	ret

setPTE:
	

	ret

;/* ADDED FOR PAGING */
GLOBAL setUpPaging

%macro pushaq 0
    push rax      ;save current rax
    push rbx      ;save current rbx
    push rcx      ;save current rcx
    push rdx      ;save current rdx
    push rbp      ;save current rbp
    push rdi       ;save current rdi
    push rsi       ;save current rsi
    push r8        ;save current r8
    push r9        ;save current r9
    push r10      ;save current r10
    push r11      ;save current r11
    push r12      ;save current r12
    push r13      ;save current r13
    push r14      ;save current r14
    push r15      ;save current r15
    push fs
    push gs
%endmacro

%macro popaq 0
    pop gs
    pop fs
    pop r15
    pop r14
    pop r13
    pop r12
    pop r11
    pop r10
    pop r9
    pop r8
    pop rsi
    pop rdi
    pop rbp
    pop rdx
    pop rcx
    pop rbx
    pop rax

%endmacro

;TODO: Send pml4 adress in eax

setCR3:

	push rbp
    mov rbp, rsp
    pushaq
    
    mov rax, cr3
    and rax, 0xFFF0000000000FFF
	or rax, 0x0000000000700000 ;Point to the 7th MB
	
    ;mov rax, 4

    mov cr3, rax

    popaq
    mov rsp, rbp
    pop rbp

    ret

refreshCR3
	
	push rbp
    mov rbp, rsp
    pushaq

	mov rax, cr3
	mov cr3, rax

    popaq
    mov rsp, rbp
    pop rbp

    ret