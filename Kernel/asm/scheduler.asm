GLOBAL createProcessContext
GLOBAL getCurrentStackPointer
GLOBAL int_schedule
GLOBAL forceInicialProcess
GLOBAL nullProcess
GLOBAL forceNext
GLOBAL int_schedule

extern switchUserToKernel
extern switchKernelToUser
extern ncPrintHex
extern kernelChecks


%macro	pushState 0
push rax
push rbx
push rcx
push rdx
push rbp
push rdi
push rsi
push r8
push r9
push r10
push r11
push r12
push r13
push r14
push r15
push fs
push gs
%endmacro

%macro	popState 0
pop gs
pop fs
pop r15
pop r14
pop r13
pop r12
pop r11
pop r10
pop r9
pop r8
pop rsi
pop rdi
pop rbp
pop rdx
pop rcx
pop rbx
pop rax
%endmacro


getCurrentStackPointer:
mov rax, rsp
ret

createProcessContext:
;Guarda el stack del proceso que lo llama en rcx
	mov rax,rsp
	;Asigna el nuevo stackrdi
	mov rsp, rdi
	;Pushea direcciones de rip y registros al stack

	push rsi

	mov rdi, rdx

	pushState
	mov rbx, rsp
	;Restaura el stack del proceso que lo llamo 
	mov rsp,rax

	mov rax, rbx

ret


forceInicialProcess:

;Asigna el nuevo stack
mov rsp, rdi

; Send end of interrupt
mov al, 0x20
out 0x20, al

;Pushea direcciones de rip y registros al stack
popState
ret


forceNext:
pushState
;Asigna el nuevo stack
mov rsp, rdi

; Send end of interrupt
;	mov al, 0x20
;	out 0x20, al

;Pushea registros al stack
	popState
	ret

int_schedule:
	pushState
; save current process's RSP
	mov rsi, rdi
	mov rdi, rsp

	; enter kernel context by setting current process's kernel-RSP
	call switchUserToKernel
;xchg bx, bx

	mov rsp, rax

	call kernelChecks

	; schedule, get new process's RSP and load it
	call switchKernelToUser

	;xchg bx, bx

	mov rsp, rax

	; Send end of interrupt
	mov al, 0x20
	out 0x20, al

popState
	ret

nullProcess:
	sti
tag:
	hlt
	jmp tag
	ret
