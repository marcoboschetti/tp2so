GLOBAL get_time
GLOBAL set_time


get_time:
    push rbp    
    mov rbp,rsp

    xor rax,rax
    mov rax,rdi			
    out 0x70,al
    in al,0x71
    mov rsp,rbp
    pop rbp
    ret


set_time:
	push rbp    
    mov rbp,rsp

    xor rax,rax
    mov rax,rdi			
    out 0x70,al
    mov rax,rsi
    out 0x71,al
    mov rsp,rbp
    pop rbp
    ret